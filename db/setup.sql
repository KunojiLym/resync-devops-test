CREATE DATABASE appdb;

\connect appdb

CREATE TABLE IF NOT EXISTS users (
    _user           TEXT UNIQUE NOT NULL,
    id              TEXT PRIMARY KEY NOT NULL,
    name            TEXT NOT NULL,
    description     TEXT
);

CREATE UNLOGGED TABLE IF NOT EXISTS users_import (values TEXT);

\copy users_import from 'users.json';

CREATE UNLOGGED TABLE IF NOT EXISTS users_preprocess (obj JSON);

INSERT INTO users_preprocess (obj)
SELECT
    regexp_split_to_table(
        replace(v, $$"}{"$$, $$"}djue748wBc,l;09{"$$),
        'djue748wBc,l;09'
    )::json
FROM (
    SELECT string_agg(values, '') as v
    FROM users_import
) s;

INSERT INTO users (_user, id, name, description) 
SELECT r._key AS _user, r.id AS id, r.name AS name, r.description AS description
FROM (
    SELECT q.key AS _key, (json_populate_record(null::users, q.value)).*
    FROM (
        SELECT (json_each(obj)).* 
        FROM users_preprocess
    ) q
) r
ON CONFLICT (id) DO 
    UPDATE SET _user = excluded._user,
               name = excluded.name,
               description = excluded.description;

DROP TABLE users_preprocess;
DROP TABLE users_import;