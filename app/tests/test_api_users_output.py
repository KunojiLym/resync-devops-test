import requests
import os

url = 'http://' + os.environ['APP_SERVER'] + ':' + os.environ['APP_PORT'] # The root url of the flask app

def assert_output_based_on_json(user_entry, comparison_entry):
    
    for user, comparison_user in zip(user_entry, comparison_entry):
        
        contents = user_entry[user]
        comparison_contents = comparison_entry[comparison_user]
        print(contents)

        assert contents['name'] == comparison_contents['name']
        assert contents['description'] == comparison_contents['description']
        assert contents['id'] == comparison_contents['id']
        assert user == comparison_user


def test_api_users_get_default_database_all_entries():

    payload = {'id' : ''}

    r = requests.get(url+'/users', params = payload)
    results = (r.json())['results']

    assert r.status_code == 200
    assert len(results) == 4

def test_api_users_get_default_database_invalid_entry():

    payload = {'id' : 'nonsense'}

    r = requests.get(url+'/users', params = payload)
    message = (r.json())['message']

    assert r.status_code == 200
    assert message == "ID not found"

def test_api_users_get_default_database_one_entry():

    payload = {'id' : 'whitewolf'}
    expected_results = {
        "geralt" : {
            "id": "whitewolf",
            "name": "Geralt of Rivia",
            "description": "Traveling monster slayer for hire"
        }
    }

    r = requests.get(url+'/users', params = payload)
    results = (r.json())['results']

    assert r.status_code == 200
    assert len(results) == 1

    assert_output_based_on_json(results, expected_results)

def test_api_users_post_new():

    payload = {
            "_user": "hell",
            "id": "onearth",
            "name": "Doom Guy",
            "description": "Shoots anything that moves"
        }
    r = requests.post(url+'/users', params = payload)
    message = (r.json())['message']

    assert r.status_code == 200
    assert message == "User with id = onearth created"

    payload = {'id': 'onearth'}
    expected_results = {
        "hell" : {
            "id": "onearth",
            "name": "Doom Guy",
            "description": "Shoots anything that moves"
        }
    }
    
    r = requests.get(url+'/users', params = payload)
    results = (r.json())['results']

    assert_output_based_on_json(results, expected_results)

def test_api_users_post_existing():

    payload = {
            "_user": "hell",
            "id": "onearth",
            "name": "Doom Guy",
            "description": "Slays anything that moves"
        }
    r = requests.post(url+'/users', params = payload)
    message = (r.json())['message']

    assert r.status_code == 200
    assert message == "index or ID already exists"

def test_api_users_update():

    payload = {
            "_user": "hell2",
            "id": "onearth",
            "name": "Doom Guy 2",
            "description": "Kills anything that moves"
        }
    
    s = requests.Session()
    req = requests.Request('UPDATE', url+'/users', params = payload)
    prepped = s.prepare_request(req)

    r = s.send(prepped)
    
    message = (r.json())['message']

    assert r.status_code == 200
    assert message == "User with id = onearth updated"

    payload = {'id': 'onearth'}
    expected_results = {
        "hell2" : {
            "id": "onearth",
            "name": "Doom Guy 2",
            "description": "Kills anything that moves"
        }
    }
    
    r = requests.get(url+'/users', params = payload)
    results = (r.json())['results']

    assert_output_based_on_json(results, expected_results)

def test_api_users_update_invalid_id():

    payload = {
            "_user": "hell2",
            "id": "onearth2",
            "name": "Doom Guy 2",
            "description": "Murders anything that moves"
        }
    s = requests.Session()
    req = requests.Request('UPDATE', url+'/users', params = payload)
    prepped = s.prepare_request(req)

    r = s.send(prepped)
    
    message = (r.json())['message']

    assert r.status_code == 200
    assert message == "user ID does not exist"

def test_api_users_delete():

    payload = {'id' : 'onearth'}

    r = requests.delete(url+'/users', params = payload)
    message = (r.json())['message']

    assert r.status_code == 200
    assert message == "User with id = onearth deleted"

def test_api_users_delete_invalid():

    payload = {'id' : 'onearth'}

    r = requests.delete(url+'/users', params = payload)
    message = (r.json())['message']

    assert r.status_code == 200
    assert message == "user ID does not exist"