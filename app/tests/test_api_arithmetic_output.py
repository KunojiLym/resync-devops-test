import requests
import os

url = 'http://' + os.environ['APP_SERVER'] + ':' + os.environ['APP_PORT'] # The root url of the flask app

def test_api_arithmetic_add():

    payload = {'x' : '1', 'y': '2', 'operation': '+'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['results'] == 3

    payload = {'x' : '-1', 'y': '5', 'operation': '+'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['results'] == 4

def test_api_arithmetic_subtract():

    payload = {'x' : '1', 'y': '2', 'operation': '-'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['results'] == -1

    payload = {'x' : '0', 'y': '5', 'operation': '-'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['results'] == -5

def test_api_arithmetic_multiply():

    payload = {'x' : '4', 'y': '2', 'operation': '*'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['results'] == 8

    payload = {'x' : '-1', 'y': '5', 'operation': '*'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['results'] == -5

def test_api_arithmetic_divide():

    payload = {'x' : '1', 'y': '2', 'operation': '/'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['results'] == 0.5

    payload = {'x' : '-1', 'y': '0', 'operation': '/'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.json()['message'] == 'division by zero'