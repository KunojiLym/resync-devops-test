import requests
import os

url = 'http://' + os.environ['APP_SERVER'] + ':' + os.environ['APP_PORT'] # The root url of the flask app

def test_api_arithmetic_post_endpoint_is_open():

    r = requests.post(url+'/arithmetic')
    assert r.status_code != 405

def test_api_arithmetic_get_endpoint_is_not_open():

    r = requests.get(url+'/arithmetic')
    assert r.status_code == 405

def test_api_user_all_endpoints_are_open():

    ## test GET
    r = requests.get(url+'/users')
    assert r.status_code != 405 

    ## test POST
    r = requests.post(url+'/users') 
    assert r.status_code != 405 

    ## test DELETE
    r = requests.delete(url+'/users')
    assert r.status_code != 405 

    ## test UPDATE
    s = requests.Session()
    req = requests.Request('UPDATE', url+'/users')
    prepped = s.prepare_request(req)

    r = s.send(prepped)

    assert r.status_code != 405
