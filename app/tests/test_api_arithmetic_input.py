import requests
import os

url = 'http://' + os.environ['APP_SERVER'] + ':' + os.environ['APP_PORT'] # The root url of the flask app

def test_api_arithmetic_invalid_format():

    payload = {'x' : 'a'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.status_code == 400

    payload = {'y' : '@'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.status_code == 400


def test_api_arithmetic_invalid_names():

    payload = {'z' : '1', 'b': '2'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.status_code == 400

def test_api_arithmetic_invalid_capital():

    payload = {'X' : '1', 'y': '2'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.status_code == 400

    payload = {'x' : '1', 'Y': '2'}
    r = requests.post(url+'/arithmetic', params = payload)

    assert r.status_code == 400