from re import error
from flask import Flask, abort
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS

import pandas as pd
import psycopg2 as pg
import os

def app_database():
    """
    Returns a connection to the app database
    """
    
    connection_args = {
        'host': os.environ['DB_HOST'],
        'dbname': os.environ['DB_NAME'], 
        'port': os.environ['DB_PORT'],
        'user': os.environ['DB_USER'], 
        'password': os.environ['DB_PASSWORD']
    }

    return pg.connect(**connection_args)

def app_execute_sql(query, connection, args = {}):
    """
    Executes queries that modify the database; returns text message of any error, or an empty string if no errors
    """

    cursor = connection.cursor()

    try:
        cursor.execute(query, args)
        connection.commit()
        error_msg = ''
    except pg.OperationalError as e:
        connection.rollback()
        error_msg = str(e)
    
    cursor.close()
    return error_msg

def create_app():
    return Flask(__name__)

application = Flask(__name__)
application.config['BUNDLE_ERRORS'] = True
api = Api(application)
CORS(application, send_wildcard=True)

#with open("./users.json", "r") as f:
#    users = json.load(f)
class arithmetic(Resource):
    def post(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('x', type=int, required=True, location='args', help='X Value must be an integer')
        self.parser.add_argument('y', type=int, required=True, location='args', help='Y Value must be an integer')
        self.parser.add_argument('operation', type=str, location='args', help='Mathematical Operators')
        self.args = self.parser.parse_args(strict=True)

        try:
            x = (self.args['x'])
            y = (self.args['y'])
            ops = (self.args['operation'])

            if ops == '-':
                return {'results': x - y}
            elif ops == '*':
                return {'results': x * y}
            elif ops == '/':
                return {'results': x / y}
            else:
                return {'results': x + y}
            
        except Exception as e:
            return {'message': str(e)}

class Users(Resource):
    def get(self):
        """
        Get Users either by id or get all Users
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id', type=str, required=False, default='', location='args', help='ID of user')
        self.args = self.parser.parse_args(strict=True)
        
        try:
            connection = app_database()

            id = (self.args['id'])
        
            if id == '':
                return_df = pd.io.sql.read_sql("SELECT * FROM users", connection).set_index("_user")
            else:  
                return_df = pd.io.sql.read_sql("SELECT * FROM users where id = %(id)s", con=connection, params={"id":id}).set_index("_user")
            
            connection.close()
        except Exception as e:
            
            connection.close()
            return {"message": str(e)}
 
        # check if record was found
        if len(return_df) == 0: 
            return {"message": "ID not found"}

        return {"results": return_df.to_dict(orient="index")}
        
    def post(self):
        """
        Create User
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('_user', type=str, required=True, location='args', help='index of user, should be unique')
        self.parser.add_argument('id', type=str, required=True, location='args', help='ID of user, should be unique')
        self.parser.add_argument('name', type=str, required=True, location='args', help='name of user')
        self.parser.add_argument('description', type=str, required=True, location='args', help='description of user')
        self.args = self.parser.parse_args(strict=True)

        args = dict()

        for arg in ["_user", "id", "name", "description"]:
            args[arg] = (self.args[arg])

        try:
            connection = app_database()
 
            # check if _user or id already exists
            count_existing = pd.io.sql.read_sql("SELECT COUNT(*) FROM users WHERE _user = %(_user)s OR id = %(id)s;", 
                                                con=connection, params=args)['count'][0]

            if count_existing > 0:
                connection.close()
                return {"message" : "index or ID already exists"}
            
            query = "INSERT INTO users (_user, id, name, description) \
                     VALUES (%(_user)s, %(id)s, %(name)s, %(description)s);"
                
            error_msg = app_execute_sql(query, connection, args)
            
            connection.close()

            if error_msg == '':
                return {'message': f"User with id = {args['id']} created"}
            else:
                abort(500, error_msg)
        
        except Exception as e:
            return {"message": str(e)}     


    def update(self):
        """
        Update User
        """
        
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('_user', type=str, required=False, location='args', help='index of user')
        self.parser.add_argument('id', type=str, required=True, location='args', help='ID of user')
        self.parser.add_argument('name', type=str, required=False, location='args', help='name of user')
        self.parser.add_argument('description', type=str, required=False, location='args', help='description of user')
        self.args = self.parser.parse_args(strict=True)
 
        args = dict()

        args['id'] = (self.args['id'])

        try:
            connection = app_database()
 
            # check if _user or id already exists
            return_df = pd.io.sql.read_sql("SELECT * FROM users WHERE id = %(id)s;", con=connection, params=args)
            if len(return_df) == 0:
                connection.close()
                return {"message": "user ID does not exist"}
                
            current_record = return_df.loc[0]
                
            for arg in ["_user", "name", "description"]:
                try:
                    args[arg] = (self.args[arg])
                except:
                    args[arg] = current_record[arg]

            query = "UPDATE users SET (_user, name, description) \
                    = (%(_user)s, %(name)s, %(description)s) \
                    WHERE id = %(id)s;"
                
            error_msg = app_execute_sql(query, connection, args)
            
            connection.close()

            if error_msg == '':
                return {'message': f"User with id = {args['id']} updated"}
            else:
                abort(500, error_msg)

        except Exception as e:
            return {"message": str(e)}       
       
    def delete(self):
        """
        Delete User
        """
        
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id', type=str, required=True, location='args', help='ID of user')
        self.args = self.parser.parse_args(strict=True)
 
        id = (self.args['id'])
        
        try:
            connection = app_database()
 
            # check if _user or id already exists
            count_existing = pd.io.sql.read_sql("SELECT COUNT(*) FROM users WHERE id = %(id)s;", con=connection, params={"id":id})['count'][0]
            if count_existing == 0:
                connection.close()
                return {"message": "user ID does not exist"}
                       
            error_msg = app_execute_sql("DELETE FROM users WHERE id = %(id)s;", connection, args={"id":id})
            
            connection.close()

            if error_msg == '':
                return {'message': f"User with id = {id} deleted"}
            else:
                abort(500, error_msg)

        except Exception as e:
            return {"message": str(e)}   
      
api.add_resource(arithmetic, '/arithmetic', methods=['POST'])
api.add_resource(Users, '/users', methods=['GET', 'POST', 'UPDATE', 'DELETE'])

if __name__ == '__main__':
    application.run(host='0.0.0.0', port=5001, threaded=True)